import React from 'react';
import './App.css';
import Header from "./Components/Headers/Header";
import Basket from "./Components/Basket/Basket";
import Favorites from "./Components/Favorites/Favorites";
import ListCards from "./Components/ListCards/ListCards";

import {BrowserRouter, Route} from "react-router-dom";

function App() {
    return (
        <BrowserRouter>
            <div>
                <Header/>
                <Route path="/listCards" render={() => <ListCards />}/>
                <Route path="/basket" render={() => <Basket />}/>
                <Route path="/favorites" render={() => <Favorites/>}/>
            </div>
        </BrowserRouter>
    );
}

export default App;
