import {useEffect, useState} from 'react';

const useProducts = () => {

    const [products, setProducts] = useState([]);
    useEffect(() => {
        fetch("/product.json")
            .then(response => response.json())
            .then(result => setProducts(result));
    }, [setProducts]);

    return {products}
};

export default useProducts;

