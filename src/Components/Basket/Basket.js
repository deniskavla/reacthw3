import React, {useEffect, useState} from "react";
import style from "../ListCards/StyleCardsList.module.css";
import ProductCard from "../Product/ProductCard";
import useProducts from "../../Hooks/useProducts";

const Basket = () => {
    const getBasket = () => JSON.parse(localStorage.getItem('Basket'));
    const {products} = useProducts();
    const [Basket, setBasket] = useState([])
    const localStBasket = () => setBasket(Array.isArray(getBasket())
        ? JSON.parse(localStorage.getItem('Basket'))
            .map(id => products.find(product => product.id === id))
        : []);

    useEffect(() => {
        if (products.length > 0) {
            localStBasket();
        }
    }, [products]);
    const delBasket = (productItem) => {
        let idToRemove = productItem.id;
        localStorage.setItem('Basket', JSON.stringify(getBasket().filter((id) => id !== idToRemove)));
        localStBasket();
    };

    return (
        <div>
            <main className={style.styleCardsList}>
                {Basket.length ?
                    Basket.map((productItem, id) =>
                        <ProductCard key={productItem.id}
                            id={productItem.id}
                            url={productItem.url}
                            nameProduct={productItem.nameProduct}
                            color={productItem.color}
                            priceProduct={productItem.priceProduct}
                            vendorCode={productItem.vendorCode}
                            delproducts={<b style={{
                                position: 'absolute',
                                top: '-15px',
                                right: '15px',
                                color: 'black',
                                padding: '15px',
                                margin: '10px'
                            }}
                                text='' key='2' onClick={() => {
                                delBasket(productItem)
                            }}/>}
                        />) : null
                }
            </main>
        </div>
    )
};

export default Basket;