import React, {useState} from 'react';
import s from './ProductCard.module.css'
import Button from "../Button/Button";
import ModalAddToCard from "../ConfirmModal/ModalAddToCard";

const getFaforitsByLocalStorage = () => JSON.parse(localStorage.getItem('Favorit'));
const getBasket = () => JSON.parse(localStorage.getItem('Basket'));

const ProductCard = (props) => {
    const [modalAddToCard, setModalAddToCard] = useState(false);
    const [isFavorit, setIsFavorit] = useState(Array.isArray(getFaforitsByLocalStorage())
        ? JSON.parse(localStorage.getItem('Favorit'))
            .find(item => item === props.id)
        : false);

    const closeModal = (e) => {

        if (e.target !== e.currentTarget) {
            return;
        }
        setModalAddToCard(false);
    };
    const addLocalStorege = () => {
        let card = props.id;
        setIsFavorit(!isFavorit);
        if (Array.isArray(getFaforitsByLocalStorage()) && getFaforitsByLocalStorage().find(item => card === item)) {
            localStorage.setItem('Favorit', JSON.stringify(getFaforitsByLocalStorage().filter(item => item !== card)));
        } else {
            if (Array.isArray(getFaforitsByLocalStorage())) {
                localStorage.setItem('Favorit', JSON.stringify([...getFaforitsByLocalStorage(), card]));
            } else {
                localStorage.setItem('Favorit', JSON.stringify([card]))
            }
        }
    };
    const addBasket = () => {
        let card = props.id;
        setIsFavorit({
                isFavorit: !setIsFavorit
            }
        );
        if (Array.isArray(getBasket()) && getBasket().find((item) => card === item)) {
            localStorage.setItem('Basket', JSON.stringify(getBasket().filter((item) => item !== card)));
        } else {
            if (Array.isArray(getBasket())) {
                localStorage.setItem('Basket', JSON.stringify([...getBasket(), card]));
            } else {
                localStorage.setItem('Basket', JSON.stringify([card]))
            }
        }
    };

    const addToCard = () => {
        setModalAddToCard({
            modalAddToCard: true,
        });
    };
    const buttonProperty = [
        {id: 1, text: 'Add to cart', backgroundColor: '#1E1E20'},
    ];
    const {url, nameProduct, color, priceProduct, vendorCode} = props;
    const modal = modalAddToCard &&
        <ModalAddToCard
            bgrndClick={closeModal}
            header={'Вы хотите добавить данный товар в корзину?'}
            actions={[
                <Button
                    style={{
                        position: 'static',
                        color: 'black',
                        padding: '19px',
                        margin: '10px'
                    }}
                    text='Да' key='1' onClick={(e) => {
                    addBasket();
                    closeModal(e);
                }}/>,
                <Button
                    style={{
                        position: 'static',
                        color: 'black',
                        padding: '15px',
                        margin: '10px'
                    }}
                    text='Нет' key='2' onClick={closeModal}/>
            ]}
        />;
    const addToBasket = <Button
        text={buttonProperty[0].text}
        buttoncolor={buttonProperty[0].backgroundColor}
        onClick={() => addToCard()}
    />;
    // {addToBasket}
    return (
        <span>
            <div className={s.productCard}>
                {props.delproducts ? '' : addToBasket}
                <span className={s.productArtikul}>Артикул {vendorCode}</span>
                <img className='img' src={url} alt=""/>
                <i onClick={addLocalStorege}
                    className={`far fa-heart ${isFavorit ? s.active : ""}`}
                >{props.favorit}</i>

                <p className={s.productPrice}>Цена: <strong> {priceProduct}</strong></p>
                <h5>{nameProduct}</h5>
                <p>Цвет:{color}</p>
                {props.delproducts && props.delproducts}
            </div>
            {modal}
        </span>
    )
};

export default ProductCard;
