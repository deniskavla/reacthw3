import React from 'react';
import ProductCard from "../Product/ProductCard";
import style from "./StyleCardsList.module.css"
import useProducts from "../../Hooks/useProducts";

const ListCards = () => {
    const {products} =  useProducts();
    return (
        <div>

            <main className={style.styleCardsList}>
                {products.length ?
                    products.map((product, id) =>
                        <ProductCard key={product.id}
                            id={product.id}
                            url={product.url}
                            nameProduct={product.nameProduct}
                            color={product.color}
                            priceProduct={product.priceProduct}
                            vendorCode={product.vendorCode}/>) : null
                }
            </main>
        </div>
    )
};

export default ListCards;

