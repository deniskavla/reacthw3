import React, {Fragment} from 'react';
import style from './ModalAddToCards.module.css'
import PropTypes from "prop-types";
import Button from "../Button/Button";

const ModalAddToCard = (props) => {

    return (
        <Fragment>
            <div className={`${style.modalOverlay} `} onClick={props.bgrndClick}>
                <div className={style.modalWindow}>
                    <div className={style.modalHeader}>
                        <div className={style.modalHeaderTitle}>
                            {props.header}
                            <div className={style.closed} onClick={props.bgrndClick}>+</div>
                        </div>
                    </div>
                    <div className={style.modalFooter}>
                        {props.actions}
                    </div>
                </div>
            </div>
        </Fragment>
    )
};

export default ModalAddToCard;
Button.propTypes = {
    bgrndClick: PropTypes.func,
    open: PropTypes.bool
};