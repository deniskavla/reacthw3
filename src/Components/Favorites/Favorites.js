import React, {Fragment, useEffect, useState} from "react";
import style from "../ListCards/StyleCardsList.module.css";
import ProductCard from "../Product/ProductCard";
import useProducts from "../../Hooks/useProducts";


const Favorit = () => {
    const getFavorit = () => JSON.parse(localStorage.getItem('Favorit'));
    const {products} = useProducts();
    const [Basket, setBasket] = useState([]);
    const localStFavorit = () => setBasket(Array.isArray(getFavorit())
        ? JSON.parse(localStorage.getItem('Favorit'))
            .map(id => products.find(product => product.id === id))
        : []);

    useEffect(() => {
        if (products.length > 0) {
            localStFavorit();
        }
    }, [products]);

    const delFavorit = (productItem) => {
        let idToRemove = productItem.id;
        localStorage.setItem('Favorit', JSON.stringify(getFavorit().filter((id) => id !== idToRemove)));
        localStFavorit();
    };
    return (
        <div>
            <main className={style.styleCardsList}>
                {Basket.length ?
                    Basket.map((productItem, id) =>
                        <ProductCard key={productItem.id}
                            id={productItem.id}
                            url={productItem.url}
                            nameProduct={productItem.nameProduct}
                            color={productItem.color}
                            priceProduct={productItem.priceProduct}
                            vendorCode={productItem.vendorCode}
                            favorit={<Fragment
                                text='' key='1'  onClick={() => {
                                delFavorit(productItem)
                            }}/>}
                        />) : null

                }
            </main>
        </div>
    )

};

export default Favorit;