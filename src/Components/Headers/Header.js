import React from 'react';
import style from './Header.module.css';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <div className={style.wrap}>
            <div className={style.header}>
                <div className={style.logo}>
                    <NavLink to="/listCards"> <img src='./logo.png' alt='logo'/> </NavLink>
                    <span>Магазин оружия</span>
                </div>
                <div className={style.link}>
                    <NavLink to="/favorites" activeClassName={style.activeLink}>Избранное</NavLink>
                    <NavLink to="/basket" activeClassName={style.activeLink}>Корзина</NavLink>
                </div>
            </div>
        </div>
    )
};

export default Header;